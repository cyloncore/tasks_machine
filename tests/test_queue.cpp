#include <thread>

#include <iostream>

#include <tasks_machine/queued_task>
#include <tasks_machine/queue>
#include <tasks_machine/queued_task_status>

#include "catch.h"

TEST_CASE("test_queued", "")
{
  tasks_machine::queue q;
  bool task1_run = false;
  bool task2_run = false;
  bool task3_run = false;
  bool task4_run = false;
  q.enqueue_task(tasks_machine::queued_task::create_single_run([&task1_run]()
  {
    task1_run = true;
    return true;
  }, [] { return false; }));
  q.enqueue_task(tasks_machine::queued_task::create_single_run([&task2_run, &task3_run, &task4_run]()
  {
    REQUIRE(task3_run);
    REQUIRE(task4_run);
    task2_run = true;
    return true;
  },
  [&task3_run, &task4_run] { return task3_run and task4_run; }));
  q.enqueue_task(tasks_machine::queued_task::create_single_run([&task3_run, &task2_run]()
  {
    REQUIRE(not task2_run);
    task3_run = true;
    return true;
  }));
  q.enqueue_task(tasks_machine::queued_task::create_single_run([&task4_run, &task2_run]()
  {
    REQUIRE(not task2_run);
    task4_run = true;
    return true;
  }));
  std::this_thread::sleep_for(std::chrono::milliseconds(100));
  REQUIRE(not task1_run);
  REQUIRE(task2_run);
  REQUIRE(task3_run);
  REQUIRE(task4_run);
}

TEST_CASE("test_recurring", "")
{
  tasks_machine::queue q;
  int counter = 0;
  REQUIRE(counter == 0);
  std::chrono::system_clock::time_point now = std::chrono::system_clock::now();
  q.enqueue_task(tasks_machine::queued_task::create_recurring([&counter]()
  {
    counter += 1;
    return true;
  }), 1000000);
  REQUIRE(counter == 0);
  std::this_thread::sleep_until(now + std::chrono::nanoseconds(200000));
  REQUIRE(counter == 0);
  std::this_thread::sleep_until(now + std::chrono::nanoseconds(1200000));
  REQUIRE(counter <= 1);
  std::this_thread::sleep_until(now + std::chrono::nanoseconds(2200000));
  REQUIRE(counter <= 2);
  std::this_thread::sleep_until(now + std::chrono::nanoseconds(4200000));
  REQUIRE(counter <= 4);
  std::this_thread::sleep_until(now + std::chrono::nanoseconds(8200000));
  REQUIRE(counter <= 8);
  q.pause();
  REQUIRE(counter <= 8);
  std::this_thread::sleep_until(now + std::chrono::nanoseconds(16200000));
  REQUIRE(counter <= 8);
  q.resume();
  std::this_thread::sleep_until(now + std::chrono::nanoseconds(32200000));
  REQUIRE(counter <= 24);
  q.stop();
}

TEST_CASE("test_resources", "")
{
  tasks_machine::queue q;
  enum Status
  {
    Unruned,
    Running,
    Stopping,
    Stopped
  };
  struct State
  {
    Status task1 = Unruned;
    Status task2 = Unruned;
    Status task3 = Unruned;
    Status task4 = Unruned;
  };
  State* state = new State;
  q.enqueue_task(tasks_machine::queued_task::create_single_run({"R1", "R2"}, [&state, &q]()
  {
    REQUIRE(q.this_thread_has_resource("R1"));
    REQUIRE(q.this_thread_has_resource("R2"));
    REQUIRE(not q.this_thread_has_resource("R3"));
    state->task1 = Running;
    while(state->task1 == Running)
    {
      std::this_thread::sleep_for(std::chrono::milliseconds(50));
    };
    state->task1 = Stopped;
    return true;
  }));
  q.enqueue_task(tasks_machine::queued_task::create_single_run({"R1"      }, [&state, &q]()
  {
    REQUIRE(q.this_thread_has_resource("R1"));
    REQUIRE(not q.this_thread_has_resource("R2"));
    REQUIRE(not q.this_thread_has_resource("R3"));
    state->task2 = Running;
    while(state->task2 == Running)
    {
      std::this_thread::sleep_for(std::chrono::milliseconds(50));
    };
    state->task2 = Stopped;
    return true;
  }));
  q.enqueue_task(tasks_machine::queued_task::create_single_run({      "R2"}, [&state, &q]()
  {
    REQUIRE(not q.this_thread_has_resource("R1"));
    REQUIRE(q.this_thread_has_resource("R2"));
    REQUIRE(not q.this_thread_has_resource("R3"));
    state->task3 = Running;
    while(state->task3 == Running)
    {
      std::this_thread::sleep_for(std::chrono::milliseconds(50));
    };
    state->task3 = Stopped;
    return true;
  }));
  q.enqueue_task(tasks_machine::queued_task::create_single_run({"R1", "R2"}, [&state, &q]()
  {
    REQUIRE(q.this_thread_has_resource("R1"));
    REQUIRE(q.this_thread_has_resource("R2"));
    REQUIRE(not q.this_thread_has_resource("R3"));
    state->task4 = Running;
    while(state->task4 == Running)
    {
      std::this_thread::sleep_for(std::chrono::milliseconds(50));
    }
    state->task4 = Stopped;
    return true;
  }));
  std::this_thread::sleep_for(std::chrono::seconds(1));
  REQUIRE(state->task1 == Running);
  REQUIRE(state->task2 == Unruned);
  REQUIRE(state->task3 == Unruned);
  REQUIRE(state->task4 == Unruned);
  state->task1 = Stopping;
  std::this_thread::sleep_for(std::chrono::seconds(1));
  REQUIRE(state->task1 == Stopped);
  REQUIRE(state->task2 == Running);
  REQUIRE(state->task3 == Running);
  REQUIRE(state->task4 == Unruned);
  state->task2 = Stopping;
  std::this_thread::sleep_for(std::chrono::seconds(1));
  REQUIRE(state->task1 == Stopped);
  REQUIRE(state->task2 == Stopped);
  REQUIRE(state->task3 == Running);
  REQUIRE(state->task4 == Unruned);
  state->task3 = Stopping;
  std::this_thread::sleep_for(std::chrono::seconds(1));
  REQUIRE(state->task1 == Stopped);
  REQUIRE(state->task2 == Stopped);
  REQUIRE(state->task3 == Stopped);
  REQUIRE(state->task4 == Running);
  state->task4 = Stopping;
  std::this_thread::sleep_for(std::chrono::seconds(1));
  REQUIRE(state->task1 == Stopped);
  REQUIRE(state->task2 == Stopped);
  REQUIRE(state->task3 == Stopped);
  REQUIRE(state->task4 == Stopped);

  delete state;
}
