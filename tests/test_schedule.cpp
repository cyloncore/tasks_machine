#include <iostream>

#include <tasks_machine/abstract_scheduled_task>
#include <tasks_machine/schedule>

#include "catch.h"

struct CounterJob : public tasks_machine::abstract_scheduled_task
{
  int counter;
  CounterJob() : counter(0)
  {
    
  }
  bool run(uint64_t) override
  {
    ++counter;
    return true;
  }
};

#define TEST(v1, v2, v3, v4, v5) \
  REQUIRE(j1.counter == v1); \
  REQUIRE(j2.counter == v2); \
  REQUIRE(j3.counter == v3); \
  REQUIRE(j4.counter == v4); \
  REQUIRE(j5.counter == v5);

TEST_CASE("test_schedule", "")
{
  tasks_machine::schedule sch;
  sch.create_time_slot(1000, 1);
  sch.create_time_slot(1000, 2);
  sch.create_time_slot(1000, 3);
  
  CounterJob j1;
  sch.assign_task(&j1, 0, 0);
  CounterJob j2;
  sch.assign_task(&j2, 1, 0);
  CounterJob j3;
  sch.assign_task(&j3, 1, 1);
  CounterJob j4;
  sch.assign_task(&j4, 2, 0);
  sch.assign_task(&j4, 2, 2);
  CounterJob j5;
  sch.assign_task(&j5, 2, 1);
  
  sch.run_slot(0);
  TEST(1,0,0,0,0)
  sch.run_slot(1);
  TEST(1,1,0,0,0)
  sch.run_slot(2);
  TEST(1,1,0,1,0)
  
  sch.run_slot(0);
  TEST(2,1,0,1,0)
  sch.run_slot(1);
  TEST(2,1,1,1,0)
  sch.run_slot(2);
  TEST(2,1,1,1,1)
  
  sch.run_slot(0);
  TEST(3,1,1,1,1)
  sch.run_slot(1);
  TEST(3,2,1,1,1)
  sch.run_slot(2);
  TEST(3,2,1,2,1)
  
  sch.run_slot(0);
  TEST(4,2,1,2,1)
  sch.run_slot(1);
  TEST(4,2,2,2,1)
  sch.run_slot(2);
  TEST(4,2,2,3,1)
  
  sch.run_slot(0);
  TEST(5,2,2,3,1)
  sch.run_slot(1);
  TEST(5,3,2,3,1)
  sch.run_slot(2);
  TEST(5,3,2,3,2)
}
