add_executable(test_tasks_machine catch.cpp test_queue.cpp test_schedule.cpp )
target_link_libraries(test_tasks_machine tasks_machine ${CMAKE_THREADS_FLAG})
add_test(NAME TEST-TASKS-MACHINE COMMAND test_tasks_machine)
