#include "abstract_queued_task"

using namespace tasks_machine;

struct abstract_queued_task::data
{
  std::list<std::string> resources;
};

abstract_queued_task::abstract_queued_task() : abstract_queued_task(std::list<std::string>())
{
}
abstract_queued_task::abstract_queued_task(const std::list<std::string>& _resources) : d(new data)
{
  d->resources = _resources;
}

abstract_queued_task::~abstract_queued_task()
{
  delete d;
}

std::list<std::string> abstract_queued_task::resources() const
{
  return d->resources;
}
