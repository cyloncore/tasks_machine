#include "../queue"

#include <mutex>
#include <condition_variable>
#include <thread>

#include "../queued_task"
#include "../worker_interface_base_p.h"

using namespace tasks_machine;

namespace tasks_machine
{
  namespace details
  {
    struct worker_interface : worker_interface_base
    {
      void add_task(const task_information& _information, uint64_t _system_time)
      {
        std::unique_lock<std::mutex> l(m);
        add_task_(_information, _system_time);
        wake_up_workers.notify_one();
      }
      std::mutex m;
      std::condition_variable wake_up_workers, stop_when_empty;
      std::size_t working_threads = 0;
      std::unordered_map<std::string, std::thread::id> resourcesHolder;

      void run_job(task_information& _ti)
      {
        _ti.task_status.d->update_status(queued_task_status::status::executing);

        {
          std::unique_lock<std::mutex> l(m);
          for(const std::string& r : _ti.task->resources())
          {
            resourcesHolder[r] = std::this_thread::get_id();
          }
        }
        _ti.task->run();

        {
          std::unique_lock<std::mutex> l(m);
          for(const std::string& r : _ti.task->resources())
          {
            resources[r] = true;
            resourcesHolder[r] = std::thread::id();
          }
        }

        if(_ti.task->finished())
        {
          delete _ti.task;
          _ti.task_status.d->update_status(queued_task_status::status::finished);
        }
        else
        {
          _ti.task_status.d->update_status(queued_task_status::status::waiting);
          add_task(_ti, _ti.next_run_time);
        }
        wake_up_workers.notify_all();
      }

    };

    struct worker
    {
      worker(worker_interface* _interface) : interface(_interface)
      {
        thread = new std::thread(&worker::run, this);
      }
      ~worker()
      {
        delete thread;
      }
      void run()
      {
        while(true)
        {
          task_information next_job;
          if(interface->paused)
          {
            std::unique_lock<std::mutex> l(interface->m);
            interface->wake_up_workers.wait(l);
          }
          {
            std::unique_lock<std::mutex> l(interface->m);
            if(not interface->running) return;
            next_job = interface->next_job();
            if(next_job.task)
            {
              interface->working_threads += 1;
            }
            else
            {
              interface->stop_when_empty.notify_all();
              if(interface->next_recurring_task < std::numeric_limits<uint64_t>::max())
              {
                interface->wake_up_workers.wait_until(l, std::chrono::time_point<std::chrono::system_clock>(std::chrono::nanoseconds(interface->next_recurring_task)));
              }
              else
              {
                interface->wake_up_workers.wait(l);
              }
            }
          }
          if(next_job.task)
          {
            interface->run_job(next_job);
            interface->working_threads -= 1;
          }
        }
      }

      std::thread* thread;
      worker_interface* interface;
    };
  }
}

struct queue::data
{
  details::worker_interface worker_interface;
  std::list<details::worker*> workers;
};

queue::queue(std::size_t _threads_count) : d(new data)
{
  for(std::size_t i = 0; i < _threads_count; ++i)
  {
    d->workers.push_back(new details::worker(&d->worker_interface));
  }
}

queue::~queue()
{
  stop();
  for(details::worker* w : d->workers)
  {
    if(w->thread->joinable())
    {
      w->thread->join();
    }
    delete w;
  }
  delete d;
}

queued_task_status queue::enqueue_task(abstract_queued_task* _job, uint64_t _recurrence, priority _priority)
{
  details::task_information ji;
  ji.task_status.d = std::make_shared<queued_task_status::data>();
  ji.task          = _job;
  ji.task_priority = _priority;
  ji.recurrence    = _recurrence;
  ji.next_run_time = 0;
  d->worker_interface.add_task(ji, system_time::now().get_time());
  return ji.task_status;
}

queued_task_status queue::enqueue_task(abstract_queued_task* _job, priority _priority)
{
  details::task_information ji;
  ji.task_status.d = std::make_shared<queued_task_status::data>();
  ji.task          = _job;
  ji.task_priority = _priority;
  d->worker_interface.add_task(ji, system_time::now().get_time());
  return ji.task_status;
}

std::function<queued_task_status(const std::function<void()>&)> queue::enqueue_task_void_function(tasks_machine::priority _priority)
{
  return [this, _priority](const std::function<void()>& _f) { return enqueue_task(queued_task::create_single_run([_f]() { _f(); return true; }), _priority);  };
}

void queue::stop()
{
  d->worker_interface.running = false;
  d->worker_interface.wake_up_workers.notify_all();
}

void queue::wake()
{
  d->worker_interface.wake_up_workers.notify_all();
}

void queue::execute_next()
{
  details::task_information next_job;
  {
    std::unique_lock<std::mutex> l(d->worker_interface.m);
    if(not d->worker_interface.running) return;
    next_job = d->worker_interface.next_job();
    if(not next_job.task)
    {
      return;
    }
  }
  d->worker_interface.run_job(next_job);
}

std::size_t queue::hardware_concurrency()
{
  return std::thread::hardware_concurrency();
}

void queue::stop_when_empty()
{
  std::unique_lock<std::mutex> l(d->worker_interface.m);
  d->worker_interface.stop_when_empty.wait(l, [this]()
  {
    return d->worker_interface.is_empty() and d->worker_interface.working_threads == 0;
  });
  d->worker_interface.running = false;
  d->worker_interface.wake_up_workers.notify_all();
}

void queue::pause()
{
  d->worker_interface.pause();
}

void queue::resume()
{
  std::unique_lock<std::mutex> l(d->worker_interface.m);
  d->worker_interface.resume();
  d->worker_interface.wake_up_workers.notify_all();
}

bool queue::is_paused() const
{
  return d->worker_interface.paused;
}

bool queue::this_thread_has_resource(const std::string& _resource) const
{
  std::unordered_map<std::string, std::thread::id>::const_iterator cit = d->worker_interface.resourcesHolder.find(_resource);
  return cit != d->worker_interface.resourcesHolder.end() and cit->second == std::this_thread::get_id();
}
