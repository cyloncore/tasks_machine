#include  "schedule_thread"

#include <condition_variable>
#include <cstring>
#include <iostream>
#include <mutex>
#include <thread>

#include "../schedule"

using namespace tasks_machine;

struct schedule_thread::data
{
  std::thread control_thread;
  tasks_machine::schedule* schedule;
  bool pause, running;
  std::mutex pause_mutex;
  std::condition_variable unpause_condition_variable;
};

schedule_thread::schedule_thread(schedule* _schedule) : d(new data)
{
  d->schedule = _schedule;
  d->pause    = false;
  d->running  = false;
}

schedule_thread::~schedule_thread()
{
  stop();
  d->control_thread.join();
  delete d;
}

void schedule_thread::start(std::size_t _rt_priority)
{
  if(d->running)
  {
    std::unique_lock<std::mutex> l(d->pause_mutex);
    d->pause    = false;
    d->unpause_condition_variable.notify_one();
  } else {
    d->pause    = false;
    d->running  = true;
    std::thread t(
      [this]() {
        std::size_t next_slot = 0;
        
        while(d->running)
        {
          {
            std::unique_lock<std::mutex> l(d->pause_mutex);
            while(d->pause)
            {
              d->unpause_condition_variable.wait(l);
            }
          }
          
          tasks_machine::schedule::run_result rr = d->schedule->run_slot(next_slot);
          next_slot = rr.next_slot;
          
          std::this_thread::sleep_for(std::chrono::nanoseconds(rr.sleep_time));
        }
      });
    std::swap(t, d->control_thread);
    
    if(_rt_priority > 0)
    {
#ifdef SCHED_RR
      sched_param sp;
      int policy;
      pthread_getschedparam(d->control_thread.native_handle(), &policy, &sp);
      sp.sched_priority = _rt_priority;
      if(pthread_setschedparam(d->control_thread.native_handle(), SCHED_RR, &sp))
      {
        std::cerr << "WARNING: Failed to set real time priority: " << std::strerror(errno) << '\n';
      }
#else
      std::cerr << "WARNING: Real-time priority is not supported on this system!\n";
#endif
    }
  }
}

void schedule_thread::stop()
{
  d->running = false;
}

void schedule_thread::pause()
{
  d->pause = true;
}
