#include "queued_task_status_p.h"

using namespace tasks_machine;

void queued_task_status::data::update_status(queued_task_status::status _new_status)
{
  std::lock_guard<std::mutex> lk(mutex);
  status = _new_status;
  switch(status)
  {
    case queued_task_status::status::invalid:
    case queued_task_status::status::waiting:
    case queued_task_status::status::executing:
      break;
    case queued_task_status::status::finished:
      cv.notify_all();
      break;
  }
}

queued_task_status::queued_task_status() : d(nullptr)
{
}

queued_task_status::queued_task_status(const queued_task_status& _rhs) : d(_rhs.d)
{
}

queued_task_status& queued_task_status::operator=(const queued_task_status& _rhs)
{
  d = _rhs.d;
  return *this;
}

queued_task_status::~queued_task_status()
{
}

queued_task_status::status queued_task_status::get_status() const
{
  return d.get() ? d->status : status::invalid;
}

void queued_task_status::wait_for_finished() const
{
  std::unique_lock<std::mutex> lk(d->mutex);
  if(d->status == status::finished) return;
  d->cv.wait(lk, [this]{ return d->status == status::finished; });
}
