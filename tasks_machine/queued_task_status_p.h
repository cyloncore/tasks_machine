#include "queued_task_status"

#include <condition_variable>
#include <mutex>

namespace tasks_machine
{
  struct queued_task_status::data
  {
    queued_task_status::status status = queued_task_status::status::waiting;
    
    void update_status(queued_task_status::status _new_status);
    mutable std::condition_variable cv;
    mutable std::mutex mutex;
  };
};
