#include "scheduled_task"

#include "abstract_scheduled_task_p.h"

using namespace tasks_machine;

struct scheduled_task::data : abstract_scheduled_task::data
{
  std::function<bool(uint64_t)> f;
};

#define D static_cast<data*>(d)

scheduled_task::scheduled_task(const std::function<bool(uint64_t)>& _f) : abstract_scheduled_task(new data)
{
  D->f = _f;
}

scheduled_task::~scheduled_task()
{
}

bool scheduled_task::run(uint64_t _time_length)
{
  return D->f(_time_length);
}
