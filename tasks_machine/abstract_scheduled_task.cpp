#include "abstract_scheduled_task_p.h"

using namespace tasks_machine;

abstract_scheduled_task::abstract_scheduled_task() : d(nullptr)
{
}

abstract_scheduled_task::abstract_scheduled_task(abstract_scheduled_task::data* _d) : d(_d)
{

}

abstract_scheduled_task::~abstract_scheduled_task()
{
  delete d;
}
