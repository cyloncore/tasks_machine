#include <assert.h>
#include <list>
#include <map>

#include <limits>
#include <unordered_map>
#include <vector>

#include "abstract_queued_task"
#include "queued_task_status_p.h"
#include "system_time"

namespace tasks_machine
{
  namespace details
  {
    struct task_information
    {
      task_information() : task(nullptr) {}
      abstract_queued_task* task;
      priority task_priority;
      queued_task_status task_status;
      uint64_t recurrence = 0;
      uint64_t next_run_time = 0;
    };
    struct worker_interface_base
    {
      worker_interface_base()
      {
        next_recurring_task = std::numeric_limits<uint64_t>::max();
        running = true;
        queues.resize((std::size_t)priority::very_low + 1);
        queues_run_count.resize((std::size_t)priority::very_low + 1);
        std::fill(queues_run_count.begin(), queues_run_count.end(), 0);
      }
      ~worker_interface_base()
      {
        for(std::vector<std::vector<task_information> >::iterator it = queues.begin(); it != queues.end(); ++it)
        {
          for(std::vector<task_information>::iterator it2 = it->begin(); it2 != it->end(); ++it2)
          {
            delete it2->task;
          }
        }
        for(std::vector<task_information>::iterator it2 = recurring_task_queue.begin(); it2 != recurring_task_queue.end(); ++it2)
        {
          delete it2->task;
        }

      }
      task_information next_job()
      {
        uint64_t current_system_time = system_time::now().get_time();
        if(next_recurring_task <= current_system_time)
        {
          next_recurring_task = std::numeric_limits<uint64_t>::max();
          for(std::vector<task_information>::iterator it = recurring_task_queue.begin(); it != recurring_task_queue.end();)
          {
            if(it->next_run_time <= current_system_time)
            {
              std::vector<tasks_machine::details::task_information> & q = queue(it->task_priority);
              q.insert(q.begin(), *it);
              it = recurring_task_queue.erase(it);
            } else {
              next_recurring_task = std::min(it->next_run_time, next_recurring_task);
              ++it;
            }
          }
        }
        for(std::size_t i = 0; i < (std::size_t)priority::very_low; ++i)
        {
          if(not queues[i].empty() and run_from_this_queue(i))
          {
            task_information ji = job_from_queue(i);
            if(ji.task)
            {
              ++queues_run_count[i];
              return ji;
            }
          }
        }
        std::fill(queues_run_count.begin(), queues_run_count.end(), 0);
        if(queue(priority::very_low).empty())
        {
          return task_information();
        } else {
          return job_from_queue((std::size_t)priority::very_low);
        }
      }
      bool run_from_this_queue(std::size_t _i)
      {
        int coef = 2;
        for(std::size_t i = _i + 1; i <= (std::size_t)priority::very_low; ++i)
        {
          if(not queues[i].empty() 
            and ((i  + 1> (std::size_t)priority::very_low )
                 or queues_run_count[_i] > coef * (queues_run_count[i+1] + 1)))
          {
            return false;
          }
        }
        return true;
      }
      task_information job_from_queue(std::size_t _i)
      {
        std::vector<task_information>& q = queues[_i];
        
        for(std::vector<task_information>::iterator it = q.begin(); it != q.end(); ++it)
        {
          task_information ji = *it;
          if(is_runnable(ji))
          {
            q.erase(it);
            for(const std::string& r : ji.task->resources())
            {
              resources[r] = false;
            }
            return ji;
          }
        }
        return task_information();
      }
      std::vector<task_information>& queue(priority _p)
      {
        return queues[(std::size_t)_p];
      }
      
      void add_task_(task_information _information, uint64_t _system_time)
      {
        _information.next_run_time = _system_time + _information.recurrence;
        if(_information.next_run_time <= _system_time)
        {
          queue(_information.task_priority).push_back(_information);
        } else {
          recurring_task_queue.push_back(_information);
          next_recurring_task = std::min(next_recurring_task, _information.next_run_time);
        }
      }
      
      bool is_runnable(const task_information& _ji)
      {
        if(not _ji.task->ready())
        {
          return false;
        }
        for(const std::string& r : _ji.task->resources())
        {
          std::unordered_map<std::string, bool>::iterator it = resources.find(r);
          if(it == resources.end())
          {
            resources[r] = true;
          } else {
            if(not resources[r])
            {
              return false;
            }
          }
        }
        return true;
      }
      bool is_empty() const
      {
        for(const std::vector<task_information>& queue : queues)
        {
          if(queue.size() > 0) return false;
        }
        return true;
      }
      void pause()
      {
        if(not paused)
        {
          paused = true;
          pause_time = system_time::now().get_time();
        }
        assert(paused);
      }
      void resume()
      {
        if(paused)
        {
          paused = false;
          uint64_t pause_delay = system_time::now().get_time() - pause_time;
          for(task_information& ti : recurring_task_queue)
          {
            ti.next_run_time += pause_delay;
          }
          next_recurring_task += pause_delay;
        }
        assert(not paused);
      }
      std::vector< std::vector<task_information> > queues;
      std::vector< task_information > recurring_task_queue;
      uint64_t next_recurring_task;
      std::vector<std::size_t> queues_run_count;
      std::unordered_map<std::string, bool> resources;
      bool running;
      bool paused = false;
      uint64_t pause_time;
    };
  }
}
