#include "schedule"

#include <cstring>
#include <limits>
#include <list>
#include <vector>

#include "abstract_scheduled_task"
#include "system_time"

using namespace tasks_machine;

namespace tasks_machine
{
  namespace details
  {
    struct slot_info
    {
      uint64_t duration;
      uint32_t period, periodicity;
      abstract_scheduled_task** jobs;
    };
  }
}

struct schedule::data
{
  std::vector<details::slot_info> slots;
};

schedule::schedule() : d(new data)
{

}

schedule::~schedule()
{
  for(details::slot_info& info : d->slots)
  {
    delete[] info.jobs;
  }
  delete d;
}

std::size_t schedule::create_time_slot(uint64_t _duration, uint32_t _periodicity)
{
  details::slot_info nsi;
  nsi.duration    = _duration;
  nsi.period      = 0;
  nsi.periodicity = std::max(uint32_t(1), _periodicity);
  nsi.jobs        = new abstract_scheduled_task*[nsi.periodicity];
  memset(nsi.jobs, 0, sizeof(abstract_scheduled_task*) * nsi.periodicity);
  d->slots.push_back(nsi);
  return d->slots.size() - 1;
}

schedule::run_result schedule::run_slot(std::size_t _slot_number)
{
  system_time time_before = system_time::now();
  details::slot_info& info = d->slots[_slot_number];
  
  abstract_scheduled_task* job = info.jobs[info.period];
  if(job)
  {
    job->run(info.duration);
  }
  
  ++info.period;
  if(info.period >= info.periodicity)
  {
    info.period = 0;
  }
  
  schedule::run_result rr;
  
  rr.next_slot = ++_slot_number;
  if(rr.next_slot >= d->slots.size())
  {
    rr.next_slot = 0;
  }
  
  system_time time_after = system_time::now();
  rr.sleep_time = info.duration - time_before.time_to(time_after);
  return rr;
}

void schedule::assign_task(abstract_scheduled_task* _task, std::size_t _slot, uint32_t _offset)
{
  d->slots[_slot].jobs[_offset] = _task;
}

abstract_scheduled_task* schedule::get_task(std::size_t _slot, uint32_t _offset)
{
  return d->slots[_slot].jobs[_offset];
}

uint64_t schedule::get_slot_duration(std::size_t _slot)
{
  return d->slots[_slot].duration;
}
