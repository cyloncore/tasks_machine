#include "queued_task"

using namespace tasks_machine;

struct queued_task::data
{
  std::function<bool()> run_f, ready_f, finished_f;
};

tasks_machine::queued_task * tasks_machine::queued_task::create_recurring(const std::function<bool ()>& _run_f, const std::function<bool ()>& _finished_f)
{
  return new queued_task(_run_f, []() { return true; }, _finished_f);
}

tasks_machine::queued_task * tasks_machine::queued_task::create_single_run(const std::function<bool ()>& _run_f, const std::function<bool ()>& _ready_f)
{
  return new queued_task(_run_f, _ready_f, []() { return true; });
}

tasks_machine::queued_task * tasks_machine::queued_task::create_single_run(const std::list<std::string>& _resources, const std::function<bool ()>& _run_f, const std::function<bool ()>& _ready_f)
{
  return new queued_task(_resources, _run_f, _ready_f, []() { return true; });
}

queued_task::queued_task(const std::list<std::string>& _resources, const std::function<bool()>& _run_f, const std::function<bool()>& _ready_f, const std::function<bool()>& _finished_f) : abstract_queued_task(_resources), d(new data)
{
  d->run_f      = _run_f;
  d->ready_f    = _ready_f;
  d->finished_f = _finished_f;
}

queued_task::queued_task(const std::function<bool()>& _run_f, const std::function<bool()>& _ready_f, const std::function<bool()>& _finished_f) : d(new data)
{
  d->run_f      = _run_f;
  d->ready_f    = _ready_f;
  d->finished_f = _finished_f;
}

queued_task::~queued_task()
{
  delete d;
}

bool queued_task::run()
{
  return d->run_f();
}


bool queued_task::finished() const
{
  return d->finished_f();
}

bool queued_task::ready() const
{
  return d->ready_f();
}
