#include <tasks_machine/system_time>

#include <chrono>

using namespace tasks_machine;

struct system_time::data
{
  data() {}
  data(const data& _rhs) : time(_rhs.time) {}
  uint64_t time;
};

system_time::system_time() : d(new data)
{

}

system_time::system_time(uint64_t _time) : d(new data)
{
  d->time = _time;
}

system_time system_time::now()
{
  std::chrono::time_point<std::chrono::system_clock> now = std::chrono::system_clock::now();
  auto duration = now.time_since_epoch();
  system_time st;
  st.d->time = std::chrono::duration_cast<std::chrono::nanoseconds>(duration).count();
  return st;
}

system_time::system_time(const system_time& _rhs) : d(_rhs.d)
{
  
}

system_time& system_time::operator=(const system_time& _rhs)
{
  d = _rhs.d;
  return *this;
}

system_time::~system_time()
{

}

uint64_t system_time::time_to(const system_time& _other) const
{
  return _other.d->time - d->time;
}

uint64_t system_time::get_time() const
{
  return d->time;
}

bool system_time::operator<(const system_time& _other) const
{
  return d->time < _other.d->time;
}
